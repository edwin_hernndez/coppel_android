package com.ehernndez.testcoppel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class ProfileActivity extends AppCompatActivity {

    Toolbar toolbar;
    TextView txttitle;
    ImageView btnBack;
    ImageView btnEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        toolbar = findViewById(R.id.toolbar_profile);
        txttitle = toolbar.findViewById(R.id.toolbar_title);
        btnBack = toolbar.findViewById(R.id.img_back);
        btnEdit = findViewById(R.id.btn_edit);
        txttitle.setText(getString(R.string.txt_informacion));

        btnBack.setOnClickListener(v -> onBackPressed());

        btnEdit.setOnClickListener(v -> {

        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}