package com.ehernndez.testcoppel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.RequestQueue;
import com.android.volley.TimeoutError;
import com.android.volley.toolbox.StringRequest;
import com.ehernndez.testcoppel.adapters.AdapterArticulos;
import com.ehernndez.testcoppel.articulos.NuevoArticuloActivity;
import com.ehernndez.testcoppel.model.ModelArticulos;
import com.ehernndez.testcoppel.singleton.Singleton;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MenuMainActivity extends AppCompatActivity {

    public static RecyclerView rvArticulos;
    public static RequestQueue requestQueue;
    public static Singleton singleton;
    JSONArray jsonArray;
    public static ProgressDialog progressDialog;
    List<ModelArticulos> articulosList;
    public static AdapterArticulos articulosAdapter;
    LinearLayout linearNotArticles;
    Toolbar toolbar;
    ExtendedFloatingActionButton btnFloating;
    SharedPreferences preferencesLogin;
    public static String idUser;
    public static String userName;
    public static String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_main);

        singleton = Singleton.getInstance(MenuMainActivity.this);
        requestQueue = singleton.getmRequestQueue();


        preferencesLogin = getSharedPreferences("dataLogin", Context.MODE_PRIVATE);
        idUser = preferencesLogin.getString("idUser", "");
        userName = preferencesLogin.getString("name", "");
        token = preferencesLogin.getString("token", "");

        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.txt_coppel));
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorTitle));
        setSupportActionBar(toolbar);
        btnFloating = findViewById(R.id.btn_floating);

        btnFloating.setOnClickListener(v -> {
            Intent intent = new Intent(MenuMainActivity.this, NuevoArticuloActivity.class);
            startActivity(intent);
        });

        rvArticulos = findViewById(R.id.rv_articulos);
        linearNotArticles = findViewById(R.id.linear_not_articles);

        singleton = Singleton.getInstance(MenuMainActivity.this);
        requestQueue = singleton.getmRequestQueue();

        rvArticulos.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MenuMainActivity.this);
        rvArticulos.setLayoutManager(linearLayoutManager);

        getArticulos();
    }

    private void getArticulos() {
        StringRequest stringRequest = new StringRequest(StringRequest.Method.GET, getString(R.string.base_url) + "getArticulosByUser/" + idUser, response -> {

            progressDialog.dismiss();

            try {
                jsonArray = new JSONArray(response);
                Log.e("SIZE ---", String.valueOf(jsonArray.length()));
                if (jsonArray.length() == 0) {
                    rvArticulos.setVisibility(View.GONE);
                    linearNotArticles.setVisibility(View.VISIBLE);
                } else {
                    linearNotArticles.setVisibility(View.GONE);
                    rvArticulos.setVisibility(View.VISIBLE);
                    llenarArticulos(jsonArray);
                    Log.e("jsonArray", jsonArray.toString());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }, error -> {
            if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                progressDialog.dismiss();

                Toast.makeText(MenuMainActivity.this, "Ocurrió un error al cargar la información", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put("Authorization", token);
                return params;
            }
        };
        requestQueue.add(stringRequest);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.txt_peticion_get));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    private void llenarArticulos(JSONArray jsonArray) {
        articulosList = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String name_card = jsonObject.getString("name");
                String description_card = jsonObject.getString("description");
                String price_card = jsonObject.getString("price");
                String id_card = jsonObject.getString("_id");
                String image = jsonObject.getString("image");

                articulosList.add(new ModelArticulos(name_card, description_card, price_card, id_card, image));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        articulosAdapter = new AdapterArticulos(articulosList);
        rvArticulos.setAdapter(articulosAdapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_profile:

                Intent intent = new Intent(MenuMainActivity.this, ProfileActivity.class);
                startActivity(intent);
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}