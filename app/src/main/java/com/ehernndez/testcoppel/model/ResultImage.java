package com.ehernndez.testcoppel.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResultImage {

    @SerializedName("message")
    @Expose
    private String messageRequest;

    @SerializedName("imageUrl")
    @Expose
    private String imageURL;

    public String getImageURL() {
        return imageURL;
    }

    public String getMessageRequest() {
        return messageRequest;
    }
}
