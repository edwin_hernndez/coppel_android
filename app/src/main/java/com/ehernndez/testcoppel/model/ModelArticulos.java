package com.ehernndez.testcoppel.model;


public class ModelArticulos {

    private String name;
    private String image;
    private String price;
    private String description;
    private String idItem;

    public ModelArticulos(String name, String description, String price, String idItem, String image) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.idItem = idItem;
        this.image = image;

    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

    public String getIdItem() {
        return idItem;
    }

    public String getDescription() {
        return description;
    }

    public String getPrice() {
        return price;
    }
}
