package com.ehernndez.testcoppel.articulos;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.os.Bundle;

import com.ehernndez.testcoppel.MenuMainActivity;
import com.ehernndez.testcoppel.R;

public class DeleteArticuloActivity extends AppCompatActivity {

    AppCompatButton btnContinuar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_articulo);

        btnContinuar = findViewById(R.id.btn_continuar);
        btnContinuar.setOnClickListener(v -> {
            Intent intent = new Intent(DeleteArticuloActivity.this, MenuMainActivity.class);
            startActivity(intent);
            finish();
        });
    }
}
