package com.ehernndez.testcoppel.articulos;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.Toolbar;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.ehernndez.testcoppel.MenuMainActivity;
import com.ehernndez.testcoppel.R;
import com.ehernndez.testcoppel.interfaceImage.ServiceImage;
import com.ehernndez.testcoppel.singleton.Singleton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import me.abhinay.input.CurrencyEditText;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class NuevoArticuloActivity extends AppCompatActivity {

    Toolbar toolbar;
    TextView txtTitle;
    ImageView btnBack;
    ImageView imgArticle;
    TextView txtImageSelect;
    TextInputEditText edtxtName;
    CurrencyEditText edtxtPrice;
    TextInputEditText edtxtDescription;
    AppCompatButton btnAddArticle;
    private RequestQueue requestQueue;
    private Singleton singleton;
    ProgressDialog progressDialog;
    LinearLayout btnUploadImage;
    ServiceImage serviceImage;
    String filePath;

    public static final int PICK_IMAGE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuevo_articulo);

        Log.e("id", MenuMainActivity.idUser);

        singleton = Singleton.getInstance(this);
        requestQueue = singleton.getmRequestQueue();

        toolbar = findViewById(R.id.toolbar_new_art);
        txtTitle = toolbar.findViewById(R.id.toolbar_title);
        btnBack = toolbar.findViewById(R.id.img_back);

        txtTitle.setText(getString(R.string.txt_new_articulo));

        edtxtName = findViewById(R.id.edtxt_nombre_articulo);
        edtxtPrice = findViewById(R.id.edtxt_price);
        edtxtDescription = findViewById(R.id.edtxt_description);
        btnAddArticle = findViewById(R.id.btn_add_product);
        txtImageSelect = findViewById(R.id.txt_image_select);
        btnUploadImage = findViewById(R.id.btn_add_image);
        imgArticle = findViewById(R.id.image_article);

        edtxtPrice.setDecimals(true);
        edtxtPrice.setSeparator(".");

        btnUploadImage.setOnClickListener(v -> {
            Intent intent = new Intent();
            intent.setType("image/jpeg");
            intent.setAction(Intent.ACTION_PICK);
            startActivityForResult(Intent.createChooser(intent, "Selecciona una imagen"), PICK_IMAGE);
        });

        btnBack.setOnClickListener(v -> {
            onBackPressed();
        });

        btnAddArticle.setOnClickListener(v -> {

            if (edtxtName.getText().equals("")) {
                Snackbar.make(v, "El nombre del producto es necesario", Snackbar.LENGTH_SHORT).show();
            } else if (edtxtPrice.getText().equals("")) {
                Snackbar.make(v, "El precio del producto es necesario", Snackbar.LENGTH_SHORT).show();
            } else if (edtxtDescription.getText().equals("")) {
                Snackbar.make(v, "La descripción del producto es necesario", Snackbar.LENGTH_SHORT).show();
            } else if (!txtImageSelect.getText().equals("Imagen seleccionada:")) {
                Snackbar.make(v, "Selecciona la imagen del producto", Snackbar.LENGTH_SHORT).show();
            } else {
                uploadImage();
            }
        });

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        serviceImage = new Retrofit.Builder().baseUrl(getString(R.string.base_url)).client(client).build().create(ServiceImage.class);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE && resultCode == Activity.RESULT_OK) {

            Uri selectedImage = data.getData();

            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);

            assert cursor != null;
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            filePath = cursor.getString(columnIndex);
            cursor.close();

            imgArticle.setImageBitmap(BitmapFactory.decodeFile(filePath));
            imgArticle.setVisibility(View.VISIBLE);
            txtImageSelect.setText(getString(R.string.txt_image_upload));
        }
    }

    private void uploadImage() {
        File file = new File(filePath);

        RequestBody reqFile = RequestBody.create(MediaType.parse("image/jpeg"), file.getAbsoluteFile());
        MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), reqFile);


        Call<ResponseBody> req = serviceImage.postImage(body, MenuMainActivity.token, "uploadImage");
        req.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    Log.e("response", jsonObject.getString("imageUrl"));
                    agregarArticulo(jsonObject.getString("imageUrl"));
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(NuevoArticuloActivity.this, "Ocurrió un error al craer el articulo", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void agregarArticulo(String urlArticulo) {
        StringRequest stringRequest = new StringRequest(StringRequest.Method.POST, getString(R.string.base_url) + "nuevoArticulo", response -> {
            progressDialog.dismiss();

            Log.e("ENTRANDO AQUI ", response);
            Intent intent = new Intent(NuevoArticuloActivity.this, ArticuloExitosoActivity.class);
            startActivity(intent);
            finish();
        }, error -> {
            progressDialog.dismiss();
            Toast.makeText(this, "Ocurrió un error al registrar el articulo, intenta nuevamente.", Toast.LENGTH_SHORT).show();
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("name", Objects.requireNonNull(edtxtName.getText()).toString());
                params.put("image", urlArticulo);
                String price = "$" + Objects.requireNonNull(edtxtPrice.getText()).toString();
                params.put("price", price);
                params.put("description", Objects.requireNonNull(edtxtDescription.getText()).toString());
                params.put("idUser", MenuMainActivity.idUser);
                return params;

            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();

                params.put("Authorization", MenuMainActivity.token);

                return params;
            }
        };
        requestQueue.add(stringRequest);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Creando articulo, por favor espere");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
