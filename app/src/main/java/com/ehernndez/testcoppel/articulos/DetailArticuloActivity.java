package com.ehernndez.testcoppel.articulos;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.ehernndez.testcoppel.MenuMainActivity;
import com.ehernndez.testcoppel.R;
import com.ehernndez.testcoppel.singleton.Singleton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class DetailArticuloActivity extends AppCompatActivity {

    TextView txtPrice;
    TextView txtID;
    TextView txtDescription;
    Toolbar toolbarDetail;
    TextView txtTitleToolbar;
    ImageView btnBack;
    private RequestQueue requestQueue;
    private Singleton singleton;
    ProgressDialog progressDialog;
    Bundle bndlArticulo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_articulo);

        bndlArticulo = getIntent().getBundleExtra("dataArticulo");


        Log.e("token", MenuMainActivity.token);
        Log.e("id", bndlArticulo.getString("idArticulo"));

        singleton = Singleton.getInstance(this);
        requestQueue = singleton.getmRequestQueue();

        txtPrice = findViewById(R.id.txt_price_art);
        txtID = findViewById(R.id.txt_id_art);
        txtDescription = findViewById(R.id.txt_description_art);
        toolbarDetail = findViewById(R.id.toolbar_detail);
        txtTitleToolbar = toolbarDetail.findViewById(R.id.toolbar_title);
        btnBack = toolbarDetail.findViewById(R.id.img_back);
        setSupportActionBar(toolbarDetail);
        btnBack.setOnClickListener(v -> onBackPressed());

        txtTitleToolbar.setText(bndlArticulo.getString("nameArticulo"));
        txtPrice.setText(bndlArticulo.getString("priceArticulo"));
        txtID.setText(bndlArticulo.getString("idArticulo"));
        txtDescription.setText(bndlArticulo.getString("descriptionArticulo"));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_delete, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_delete:


                new MaterialAlertDialogBuilder(this)
                        .setTitle("Coppel")
                        .setMessage("¿Deseas eliminar el articulo?")
                        .setPositiveButton("Eliminar", (dialog, which) -> {
                            deleteArticulo();
                        })
                        .setNegativeButton("Cancelar", null)
                        .show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void deleteArticulo() {
        StringRequest stringRequest = new StringRequest(StringRequest.Method.POST, getString(R.string.base_url) + "deleteArticulo", response -> {
            progressDialog.dismiss();

            Log.e("ENTRANDO AQUI ", response);
            Intent intent = new Intent(DetailArticuloActivity.this, DeleteArticuloActivity.class);
            startActivity(intent);
            finish();
        }, error -> {
            progressDialog.dismiss();
            Toast.makeText(this, "Ocurrió un error al eliminar el articulo, intenta nuevamente.", Toast.LENGTH_SHORT).show();
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                String idArticulo = bndlArticulo.getString("idArticulo");
                params.put("idArticulo", Objects.requireNonNull(idArticulo));
                return params;

            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();

                params.put("Authorization", MenuMainActivity.token);
                params.put("Content-Type", "application/x-www-form-urlencoded");

                return params;
            }
        };
        requestQueue.add(stringRequest);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("eliminando articulo, por favor espere");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}