package com.ehernndez.testcoppel.registroUsuario;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.ehernndez.testcoppel.R;
import com.ehernndez.testcoppel.singleton.Singleton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class RegistroUsuarioActivity extends AppCompatActivity {

    Toolbar toolbar;
    TextView txtTitle;
    ImageView btnBack;
    TextInputEditText edtxtName;
    TextInputEditText edtxtLastName;
    TextInputEditText edtxtEmail;
    TextInputEditText edtxtPassword;
    TextInputEditText edtxtConfirmPassword;
    AppCompatButton btnCreateAccount;

    private RequestQueue requestQueue;
    private Singleton singleton;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_usuario);

        toolbar = findViewById(R.id.toolbar);
        txtTitle = toolbar.findViewById(R.id.toolbar_title);
        btnBack = toolbar.findViewById(R.id.img_back);

        txtTitle.setText(getString(R.string.txt_create_account));
        btnBack.setOnClickListener(v -> onBackPressed());

        singleton = Singleton.getInstance(this);
        requestQueue = singleton.getmRequestQueue();

        edtxtName = findViewById(R.id.edtxt_name);
        edtxtLastName = findViewById(R.id.edtxt_last_name);
        edtxtEmail = findViewById(R.id.edtxt_email);
        edtxtPassword = findViewById(R.id.edtxt_password);
        edtxtConfirmPassword = findViewById(R.id.edtxt_confirm_password);

        btnCreateAccount = findViewById(R.id.btn_create_account);
        btnCreateAccount.setOnClickListener(v -> {

            if (edtxtName.getText().toString().equals("")) {
                Snackbar.make(v, "El nombre es necesario", Snackbar.LENGTH_SHORT).show();
            } else if (edtxtLastName.getText().toString().equals("")) {
                Snackbar.make(v, "El apellido es necesario", Snackbar.LENGTH_SHORT).show();
            } else if (edtxtEmail.getText().toString().equals("")) {
                Snackbar.make(v, "El email es necesario", Snackbar.LENGTH_SHORT).show();
            } else if (edtxtPassword.getText().toString().equals("")) {
                Snackbar.make(v, "El password es necesario", Snackbar.LENGTH_SHORT).show();
            } else if (edtxtConfirmPassword.getText().toString().equals("")) {
                Snackbar.make(v, "Es necesario confirmar el password", Snackbar.LENGTH_SHORT).show();
            } else if (!edtxtPassword.getText().toString().equals(edtxtConfirmPassword.getText().toString())) {
                Snackbar.make(v, "La contraseña no coincide", Snackbar.LENGTH_SHORT).show();
            } else {
                createAccount();
            }
        });
    }

    private void createAccount() {
        StringRequest stringRequest = new StringRequest(StringRequest.Method.POST, getString(R.string.base_url) + "registerUser", response -> {
            progressDialog.dismiss();
            Log.e("response", response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                String userName = jsonObject.getString("userName") + " " + jsonObject.getString("lastName");

                Intent intent = new Intent(RegistroUsuarioActivity.this, RegistroExitosoActivity.class);
                Bundle bndlUser = new Bundle();
                bndlUser.putString("name", userName);
                intent.putExtra("userName", bndlUser);
                startActivity(intent);
                finish();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }, error -> {
            progressDialog.dismiss();
            Toast.makeText(this, "Ocurrió un error al iniciar la sesión, intenta nuevamente.", Toast.LENGTH_SHORT).show();
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("name", Objects.requireNonNull(edtxtName.getText()).toString());
                params.put("lastName", Objects.requireNonNull(edtxtLastName.getText()).toString());
                params.put("mail", Objects.requireNonNull(edtxtEmail.getText()).toString());
                params.put("password", Objects.requireNonNull(edtxtConfirmPassword.getText()).toString());
                return params;
            }
        };
        requestQueue.add(stringRequest);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.txt_login_wait));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}