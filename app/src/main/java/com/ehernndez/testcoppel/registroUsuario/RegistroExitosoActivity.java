package com.ehernndez.testcoppel.registroUsuario;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.ehernndez.testcoppel.LoginActivity;
import com.ehernndez.testcoppel.R;

public class RegistroExitosoActivity extends AppCompatActivity {

    TextView txtUsername;
    Bundle bndlName;
    String name;
    AppCompatButton btnContinuar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_exitoso);

        txtUsername = findViewById(R.id.txt_username_register);
        bndlName = getIntent().getBundleExtra("userName");
        name = bndlName.getString("name");
        txtUsername.setText(name);
        btnContinuar = findViewById(R.id.btn_continuar_login);

        btnContinuar.setOnClickListener(v -> {
            Intent intent = new Intent(RegistroExitosoActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        });
    }
}