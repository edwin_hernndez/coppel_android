package com.ehernndez.testcoppel.interfaceImage;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Url;

public interface ServiceImage {

    @Multipart
    @POST
    Call<ResponseBody> postImage(@Part MultipartBody.Part image, @Header("Authorization") String auth, @Url String Url);
}
