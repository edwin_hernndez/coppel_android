package com.ehernndez.testcoppel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.ehernndez.testcoppel.registroUsuario.RegistroUsuarioActivity;
import com.ehernndez.testcoppel.singleton.Singleton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class LoginActivity extends AppCompatActivity {

    TextInputEditText edtxtMail;
    TextInputEditText edtxtPassword;
    TextView txtCreateAccount;
    AppCompatButton btnLogin;
    private RequestQueue requestQueue;
    private Singleton singleton;
    ProgressDialog progressDialog;

    SharedPreferences preferencesLogin;
    SharedPreferences.Editor editorLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        singleton = Singleton.getInstance(this);
        requestQueue = singleton.getmRequestQueue();

        preferencesLogin = getSharedPreferences("dataLogin", Context.MODE_PRIVATE);

        edtxtMail = findViewById(R.id.edtxt_correo);
        edtxtPassword = findViewById(R.id.edtxt_password);
        txtCreateAccount = findViewById(R.id.btn_create_acocunt);
        btnLogin = findViewById(R.id.btn_login);

        btnLogin.setOnClickListener(v -> {
            if (edtxtMail.getText().toString().equals("")) {
                Snackbar.make(v, "El ic_email es necesario", Snackbar.LENGTH_SHORT).show();
            } else if (edtxtPassword.getText().toString().equals("")) {
                Snackbar.make(v, "El password es necesario", Snackbar.LENGTH_SHORT).show();
            } else {
                login();
            }
        });

        txtCreateAccount.setOnClickListener(v -> {
            Intent intent = new Intent(this, RegistroUsuarioActivity.class);
            startActivity(intent);
        });
    }

    private void login() {
        StringRequest stringRequest = new StringRequest(StringRequest.Method.POST, getString(R.string.base_url) + "login", response -> {
            progressDialog.dismiss();
            Log.e("response", response);
            try {
                JSONObject jsonObject = new JSONObject(response);

                String name = jsonObject.getString("name") + " " + jsonObject.getString("lastName");
                editorLogin = preferencesLogin.edit();
                editorLogin.putString("idUser", jsonObject.getString("_id"));
                editorLogin.putString("mail", jsonObject.getString("mail"));
                editorLogin.putString("token", jsonObject.getString("token"));
                editorLogin.putString("name", name);
                editorLogin.apply();


                Intent intent = new Intent(LoginActivity.this, MenuMainActivity.class);
                startActivity(intent);
                finish();

            } catch (JSONException e) {
                e.printStackTrace();
                progressDialog.dismiss();
                Toast.makeText(this, "Ocurrió un error, intenta nuevamente.", Toast.LENGTH_SHORT).show();
            }
        }, error -> {
            progressDialog.dismiss();
            Toast.makeText(this, "Ocurrió un error al iniciar la sesión, intenta nuevamente.", Toast.LENGTH_SHORT).show();
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("mail", Objects.requireNonNull(edtxtMail.getText()).toString());
                params.put("password", Objects.requireNonNull(edtxtPassword.getText()).toString());
                return params;
            }
        };
        requestQueue.add(stringRequest);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.txt_login_wait));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}