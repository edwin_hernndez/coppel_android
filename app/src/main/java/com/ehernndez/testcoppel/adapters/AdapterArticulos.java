package com.ehernndez.testcoppel.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.ehernndez.testcoppel.R;
import com.ehernndez.testcoppel.articulos.DetailArticuloActivity;
import com.ehernndez.testcoppel.model.ModelArticulos;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AdapterArticulos extends RecyclerView.Adapter<AdapterArticulos.NewsViewHolder> {

    Context context;
    List<ModelArticulos> articles;

    public AdapterArticulos(List<ModelArticulos> articulosList) {
        this.articles = articulosList;

    }

    @NonNull
    @Override
    public AdapterArticulos.NewsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_articulo, parent, false);
        return new NewsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterArticulos.NewsViewHolder holder, int position) {
        holder.txtName.setText(articles.get(position).getName());
        holder.txtPrice.setText(articles.get(position).getPrice());
        holder.txtId.setText(articles.get(position).getIdItem());
        holder.txtDescription.setText(articles.get(position).getDescription());
        String imagereq = articles.get(position).getImage();
        String image = imagereq.replace("\\", "");
        Uri uri = Uri.parse(image);
        Log.e("imageUrl", image);
        Picasso.get().load(uri).into(holder.imgArticle);

        Log.e("idart ---", articles.get(position).getIdItem());

        context = holder.context;

        holder.cardArticulo.setOnClickListener(v -> {
            Intent intent = new Intent(context, DetailArticuloActivity.class);
            Bundle dataArticulo = new Bundle();
            dataArticulo.putString("nameArticulo", holder.txtName.getText().toString());
            dataArticulo.putString("priceArticulo", holder.txtPrice.getText().toString());
            dataArticulo.putString("idArticulo", holder.txtId.getText().toString());
            dataArticulo.putString("descriptionArticulo", holder.txtDescription.getText().toString());
            intent.putExtra("dataArticulo", dataArticulo);
            context.startActivity(intent);
        });

    }

    @Override
    public int getItemCount() {
        return articles.size();
    }

    public class NewsViewHolder extends RecyclerView.ViewHolder {

        TextView txtName;
        TextView txtPrice;
        TextView txtId;
        TextView txtDescription;
        Context context;
        ImageView imgArticle;
        CardView cardArticulo;

        public NewsViewHolder(@NonNull View itemView) {
            super(itemView);

            txtName = itemView.findViewById(R.id.txt_name);
            txtPrice = itemView.findViewById(R.id.txt_price);
            txtId = itemView.findViewById(R.id.txt_id);
            txtDescription = itemView.findViewById(R.id.txt_description);
            imgArticle = itemView.findViewById(R.id.img_article);
            cardArticulo = itemView.findViewById(R.id.card_articulo);
            context = itemView.getContext();
        }
    }
}